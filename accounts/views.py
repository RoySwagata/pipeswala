from http.client import HTTPResponse

from django.contrib.auth import login, authenticate
from django.http import HttpResponse
from django.shortcuts import render, redirect
from distributor.models import Distributor
from distributor.views import distributor_ashboard_view
from dealer.models import Dealer
from dealer.views import dealer_dashboard_view
from customer.views import customer_view


# Create your views here.

def login_view(request):
    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user is not None:
            login(request, user)
            print(user.id)
            if Distributor.objects.filter(user_id=user.id).exists():
                return redirect('dist_dashboard_page')
            elif Dealer.objects.filter(user_id=user.id).exists():
                return redirect('dashboard_page')
            else:
                return redirect('customer_dashboard_page')
        else:
            print("No user found")
    return render(request, 'login.html', {})


def register_view(request):
    return render(request, 'register.html', {})


def home_view(request):
    return render(request, 'index.html', {})
