from django.shortcuts import render
from django.contrib.auth.decorators import login_required

# Create your views here.

def demo_view(request):
    return render(request, 'demo.html', {})


@login_required
def distributor_ashboard_view(request):
    return render(request, 'dashboard.html', {})
