from django.db import models
from django.contrib.auth.models import User

# Create your models here.


class Distributor(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    region = models.CharField(max_length=150)

    def __str__(self):
        return self.user.username
