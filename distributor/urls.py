from django.urls import path
from .views import demo_view,distributor_ashboard_view
urlpatterns = [
    path('', distributor_ashboard_view, name ='dist_dashboard_page'),
    path('demo/', demo_view, name ='demo_page')
]
