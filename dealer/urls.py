from django.urls import path
from .views import dealer_dashboard_view
urlpatterns = [
    path('', dealer_dashboard_view, name ='dashboard_page'),
]
