from django.shortcuts import render


# Create your views here.

def dealer_dashboard_view(request):
    return render(request, 'dealer_dashboard.html', {})
