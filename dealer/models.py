from django.db import models
from distributor.models import Distributor
from django.contrib.auth.models import User


# Create your models here.

class Dealer(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    region = models.CharField(max_length=150)
    distributor = models.ForeignKey(Distributor, on_delete=models.CASCADE)

    def __str__(self):
        return self.user.username
