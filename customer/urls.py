from django.urls import path
from .views import customer_view
urlpatterns = [
    path('', customer_view, name='customer_dashboard_page'),
]
